#!/usr/bin/env bats

@test "PermitRootLogin was set to 'no' in /etc/ssh/sshd_config" {
  run grep -q -E "^PermitRootLogin no$" /etc/ssh/sshd_config

  [ "$status" -eq 0 ]
}

@test "PasswordAuthentication was set to 'no' in /etc/ssh/sshd_config" {
  run grep -q -E "^PasswordAuthentication no$" /etc/ssh/sshd_config

  [ "$status" -eq 0 ]
}

@test "sudoers group is disabled" {
  run grep -q -v -E "^%sudo" /etc/sudoers

  [ "$status" -eq 0 ]
}
