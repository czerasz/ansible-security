#!/bin/bash

# Start ssh server
/usr/sbin/sshd

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
  echo "** Trapped CTRL-C"
}

sleep infinity & wait
