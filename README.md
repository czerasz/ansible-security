# Ansible Security Role

A role used to improve security on your server.

## Installation

Install the role with:

```
git clone https://gitlab.com/czerasz/ansible-security.git /etc/ansible/roles/czerasz.security
```

## Usage

```
---

- name: Improve security
  hosts: all
  roles:
    - role: czerasz.security
      vars:
        allow_root_login: no
        allow_password_authentication: no
        remove_sudo_group_rights: yes
```

## Role Variables

| name | description | default |
| --- | --- | --- |
| `allow_root_login` | Don't allow the root user to login to your server. Every system has a root user. Bad people know this so they try to guess it's password using brute force | `no` |
| `allow_password_authentication` | Don't allow people to login via SSH using their password. The recommended way is using SSH keys | `no` |
| `remove_sudo_group_rights` | Don't use the `sudo` group | `yes` |

# Test

Requirements:

- [Docker Compose](https://docs.docker.com/compose/)

Run tests with:

```
docker-compose up
```
